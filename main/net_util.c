#include "net_util.h"
#include "util.h"

int net_dns_lookup(const char* addr, const char *port,
		struct addrinfo** addr_info)
{
	int err;
	const struct addrinfo hints = {
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};

	err = getaddrinfo(addr, port, &hints, addr_info);

	if(err || !*addr_info) {
		LOGE("DNS lookup failure %d\n", err);
		if(*addr_info) {
			freeaddrinfo(*addr_info);
		}
		return -1;
	}

	return 0;
}
